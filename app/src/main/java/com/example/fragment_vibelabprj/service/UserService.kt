package com.example.fragment_vibelabprj.service

import android.content.Context
import com.example.fragment_vibelabprj.data.StorageAccessor
import com.example.fragment_vibelabprj.data.UserData

object UserService {
    fun getUserData(): UserData? {
        return StorageAccessor.getStorageData().userData
    }

    fun isLoggedIn (): Boolean {
        return getUserData() !== null
    }
}