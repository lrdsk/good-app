package com.example.fragment_vibelabprj.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.example.fragment_vibelabprj.R
import com.example.fragment_vibelabprj.data.StorageAccessor
import com.example.fragment_vibelabprj.data.StorageData
import com.example.fragment_vibelabprj.data.UserData
import com.example.fragment_vibelabprj.databinding.FragmentRegistrationBinding
import com.example.fragment_vibelabprj.service.UserService

class RegistrationFragment : Fragment() {
    lateinit var bindingClass: FragmentRegistrationBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        bindingClass = FragmentRegistrationBinding.inflate(inflater)

        bindingClass.buttonGoToLogin.setOnClickListener {
            Navigation.findNavController(bindingClass.root)
                .navigate(R.id.navigateToLoginFragment)
        }

        return bindingClass.root

//        StorageAccessor.saveStorageData(StorageAccessor.getStorageData().copy(
//            token = "token-from-backend",
//            userData = UserData("tetsUser")
//        ))
//
//        UserService.getUserData()!!.name
    }

    companion object {
        @JvmStatic
        fun newInstance() = RegistrationFragment()
    }
}