package com.example.fragment_vibelabprj

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.fragment_vibelabprj.databinding.ActivityMainBinding
import com.example.fragment_vibelabprj.databinding.FragmentWebBinding

class MainActivity : AppCompatActivity() {
    lateinit var bindingClass: ActivityMainBinding
    lateinit var bindingWebFragmentWebBinding: FragmentWebBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindingClass = ActivityMainBinding.inflate(layoutInflater)
        bindingWebFragmentWebBinding = FragmentWebBinding.inflate(layoutInflater)
        setContentView(bindingClass.root)
    }

    override fun onBackPressed() {
        if (bindingWebFragmentWebBinding.wbWebView.canGoBack()) bindingWebFragmentWebBinding.wbWebView.goBack()
        else super.onBackPressed()
    }
}