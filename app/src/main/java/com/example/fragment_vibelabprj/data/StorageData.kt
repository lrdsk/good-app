package com.example.fragment_vibelabprj.data

data class StorageData(
    val token: String?,
    val userData: UserData?
) {
    companion object {
        val DEFAULT = StorageData(null, null)
    }
}

data class UserData(
    val name: String,
)