package com.example.fragment_vibelabprj.data

import android.content.Context
import com.example.fragment_vibelabprj.App
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory

object StorageAccessor {
    private var storageData: StorageData? = null
    private val moshi = Moshi.Builder()
        .addLast(KotlinJsonAdapterFactory())
        .build()
    private val jsonAdapter = moshi.adapter(StorageData::class.java)

    fun getStorageData(): StorageData {
        if (this.storageData === null) {
            this.reloadStorageData()
        }
        return this.storageData!!
    }

    fun reloadStorageData() {
        try {
            val jsonString = App.getAppContext()
                .openFileInput("data.json")
                .use { it.readBytes() }
                .toString(Charsets.UTF_8)
            this.storageData = this.jsonAdapter.fromJson(jsonString)!!
        } catch (e: Throwable) {
            saveStorageData( StorageData.DEFAULT)
        }
    }

    fun saveStorageData( data: StorageData) {
        this.storageData = data
        try {
            val jsonString = this.jsonAdapter.toJson(storageData)
            App.getAppContext().openFileOutput("data.json", Context.MODE_PRIVATE).use {
                it.write(jsonString.toByteArray(Charsets.UTF_8))
            }
        } catch (e: Throwable) {
            System.err.println("Failed to save data: ${data}")
            e.printStackTrace()
        }
    }
}